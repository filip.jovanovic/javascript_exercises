function task13(users, obligations){
    const result = {};

    users.sort((a,b) => a.id - b.id);
    obligations.sort((a,b) => a.userId - b.userId);

    let current_user_id = users[0].id;
    let current_user_name = users[0].name;
    let counter = 0;
    result[current_user_name] = {'completed': [], 'ongoing': []};

    for(let obligation of obligations){
        while(obligation.userId != current_user_id){
            current_user_id = users[++counter].id;
            current_user_name = users[counter].name;
            result[current_user_name] = {'completed': [], 'ongoing': []};
        }
        
        obligation.completed ? result[current_user_name].completed.push(obligation) : result[current_user_name].ongoing.push(obligation);
    }
    
    for(let user in result){
        console.log(user, result[user]);
    }

    return result;
};

// async function getArrays(url_users, url_obligations){
//     const res_users = await fetch(url_users);
//     const data_users = await res_users.json();

//     const res_obligations = await fetch(url_obligations);
//     const data_obligations = await res_obligations.json();

//     return [data_users, data_obligations];
// };

async function getOneArray(url){
    const res = await fetch(url);
    const data = await res.json();
    return data;
}


// getArrays('https://jsonplaceholder.typicode.com/users', 'https://jsonplaceholder.typicode.com/todos').then(([data1, data2]) => task13(data1, data2));

let users;
let obligations;
(async () => {
    users = await getOneArray('https://jsonplaceholder.typicode.com/users');
    obligations = await getOneArray('https://jsonplaceholder.typicode.com/todos');
    task13(users, obligations);
})();
