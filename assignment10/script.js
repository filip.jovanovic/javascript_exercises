const addBtn = document.querySelector('#addBtn');
const container = document.querySelector('.container');

addBtn.addEventListener('click', onBtnClick);

function onBtnClick(e) {
    const newDiv = document.createElement('div');
    newDiv.style.background = 'blue';
    newDiv.style.width = '40px';
    newDiv.style.height = '15px';
    container.appendChild(newDiv);

    newDiv.addEventListener('click', onDivClick);
};

function onDivClick(e){
    const color = e.target.style.background === 'red' ? 'blue' : 'red';
    e.target.style.background = color;
    e.target.style.transition = '2s';
}
