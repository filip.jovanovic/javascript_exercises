let cars = [
    { brand: "Toyota", color: "Red", year: 2018 },
    { brand: "Honda", color: "Gray", year: 2020 },
    { brand: "Ford", color: "White", year: 2019 }
];

// 1. Displays from cars (console.log) in one line:
// Brand from the first object, color from the second object and year from the third.
console.log(cars[0].brand, cars[1].color, cars[2].year);


let carsDetails = [
    {
      "color": "Red",
      "brand": "Toyota",
      "registration": false,
      "capacity": 5,
      "passengers": ["Jonathan", "Mark", "Ana"]
    },
    {
      "color": "Gray",
      "brand": "Honda",
      "registration": true,
      "capacity": 5,
      "passengers": ["Ben", "Joe"]
    }];
 
//2. From the array carsDetails displays from the first object: brand, capacity and second passenger.
// From another object, display: brand, color and the last two passengers.
console.log(carsDetails[0].brand, carsDetails[0].capacity, carsDetails[0].passengers[1]);
console.log(carsDetails[1].brand, carsDetails[1].color, carsDetails[1].passengers.slice(-2));
