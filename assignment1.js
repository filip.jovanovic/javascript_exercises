let products = ["TV", "laptop", "earphones"];

// 1. Add "telephone" and "lighter" to products
products.push('telephone', 'lighter');
console.log(products);

// 2. Remove the first and last element from the products array
products.shift();
products.pop();
console.log(products);

// 3. Replace a specific product with a new one ("vacuum cleaner") using the splice method.
products.splice(0, 1, 'vacuum cleaner');
console.log(products);

// 4. Create a new array newProducts and merge it with the existing array.
let newProducts = ['computer', 'keyboard', 'pen'];
let mergedProducts = products.concat(newProducts);
console.log(mergedProducts);
