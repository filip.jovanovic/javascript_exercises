// 1. Write a function that takes a string as a parameter and the function should return a string with the vowels removed.
// Print function result.

const task1 = param => param.replace(/[aeiouAEIOU]/g, '');

console.log(task1('Hello World'));


// 2. Write a function that is passed a single or double digit number.
// If the number is two-digit, just return it, and if it is one-digit,
// add a zero in front of the number and then return such a number. The function returns a string.

const task2 = number => number >= 10 ? number.toString() : '0'.concat(number);

console.log(task2(14));
console.log(task2(9));
