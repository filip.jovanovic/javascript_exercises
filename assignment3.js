// Using loop iterate through the digits (numbers) and adds them together to find the sum.
const numbers = [1,2,3,5,9];
let sum = 0;
for(let number of numbers){
    sum += number;
}
console.log(sum);
