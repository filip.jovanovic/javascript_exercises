async function getUsers(){
    const res = await fetch('https://jsonplaceholder.typicode.com/users');
    const data = await res.json();
    const result = data.map(user => `${user.name} (${user.username})`);
    console.log(result);
    // return result;
};

getUsers();
