async function getUsers(){
    const res = await fetch('https://jsonplaceholder.typicode.com/users');
    const data = await res.json();
    const group = [];
    for(let user of data){
        let extension = user.email.substring(user.email.lastIndexOf('.')+1);

        let extension_exists = false;
        for(let users_object of group){
            if(users_object[extension]){
                extension_exists = true;
                users_object[extension].push(user);
                break;
            }
        }

        if(!extension_exists){
            let obj = {};
            obj[extension] = [user];
            group.push(obj);
        }

    }

    for(let users of group){
        console.log(users);
    }
};

getUsers();
