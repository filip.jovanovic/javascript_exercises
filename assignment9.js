const students = [
    { first_name: 'Alice', last_name: 'Smith', subject: 'Math', marks: 80 },
    { first_name: 'Bob', last_name: 'Smith', subject: 'Math', marks : 45 },
    { first_name: 'Charlie', last_name: 'Johnson', subject: 'Physics', marks: 70 },
    { first_name: 'Dave', last_name: 'Williams', subject: 'Physics', marks: 30 },
];

const studentsPassed =
    students.filter(student => student.marks >= 50)
    .map(student => `${student.first_name} ${student.last_name} - ${student.subject}`);

console.log(studentsPassed);


const products = [
    { name: 'Shirt', price: 20, inStock: true },
    { name: 'Pants', price: 30, inStock: false },
    { name: 'Shoes', price: 50, inStock : true },
    { name: 'Hat', price: 10, inStock: true },
];

const productsInStock = products.filter(product => product.inStock);
console.log(productsInStock);