const numbers = [1, 2, 3, 4, 5];


function doubleNumbers(arr) {
    return arr.map(num => num*2);
};

function filterEvenNumbers(arr) {
    return arr.filter(num => num % 2 == 0);
};

function sumNumbers(arr) {
    return arr.reduce((num, sum) => num + sum, 0);
};

const doubled = doubleNumbers(numbers);
console.log(doubled);

const filtered = filterEvenNumbers(doubled);
console.log(filtered);

const sum = sumNumbers(filtered);
console.log(sum);
