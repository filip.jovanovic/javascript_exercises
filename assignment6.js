// Create a constructor function Book that initializes book objects with title, author, and year properties.
// Additionally, add a method summary to the prototype of Book that displays the book's details in a formatted string.

function Book(title, author, year){
    this.title = title;
    this.author = author;
    this.year = year;
};

Book.prototype.summary = function() {
    return `${this.title}, ${this.author}, ${this.year}`;
};

const book1 = new Book('Dostojevski', 'Zlocin i kazna', 2020);
console.log(book1.summary());
