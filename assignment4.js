// Create a function that prints numbers from 1 to a given number.
// But for multiples of 3, print "Fizz" instead of the number, and for the multiples of 5, print "Buzz".
// For numbers which are multiples of both 3 and 5, print "FizzBuzz".

function task4(given_num){
    for(let i = 1; i < given_num; i++){
        if(i % 15 == 0)
            console.log('FizzBuzz');
        else if(i % 3 != 0 && i % 5 != 0)
            console.log(i);
        else
            i % 3 == 0 ? console.log('Fizz') : console.log('Buzz');;
    }
};

task4(20);
