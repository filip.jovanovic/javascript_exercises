class Rectangle{
    constructor(width, height){
        this.width = width;
        this.height = height;
    };
    calculateArea(){
        return this.width * this.height;
    };
};

const myRect = new Rectangle(5, 10);
console.log(myRect.calculateArea());
